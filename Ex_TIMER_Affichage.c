/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */



#define _XTAL_FREQ 20000000L
uint16_t counter;                   //initialisaiton du compteur

 /* Tableau dont chaque case correspond � l'affichage d'un num�ro */
uint8_t Digit[]={0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F};


void main(void)
{   
    TRISD = 0;
    TRISE = 0x00;   
    LATE = 0;                      //choix du "panneau" d'affichage � 0
    LATD = 0;                      //initialisation des LEDS comme sortie
    
    T2CONbits.TMR2ON= 1;          //enclenche le timer
    T2CONbits.T2CKPS=0b01;        //r�glage de la f�quence de pulsation � 1000Hz
    PR2= 249;
    T2CONbits.T2OUTPS= 0b0100;
    
    TMR2IE=1;                      //autorise l'interruption
    PEIE = 1;
    GIE = 1;
    for(;;)                        //boucle infinie du main (sinon s'arr�te)
    {}
}

uint16_t second;                   //initialisation des variables
uint8_t digit;
    void interrupt ISR (void)
    {   
       
        if (TMR2IF==1)             //compteur de 1000 pulsations = 1 sec      
        {
            TMR2IF=0;
            counter ++;
        
            if((counter % 5) == 0)
            {
              //commande switch: indique le cas � suivre suivant une variable
              //on change de cas chaque 5ms pour que l'oeil ne voit pas de chgt
              switch(digit)     
              {
                case 0:
                  LATE= 0b111;    
                  LATD= Digit[second/100];
                  LATE = 0b110;  
                break;
                case 1:
                  LATE = 0b111;
                  LATD = Digit[second%100/10];
                  LATE = 0b101;
                break;
                case 2:
                  LATE = 0b111;
                  LATD = Digit[second%10];
                  LATE = 0b011;               
                }
                digit = (digit + 1) % 3;
                //la variable digit varie entre 0 1 2 
            }
            
            if(counter==1000)
            {
                second++;       //incr�ementation des sec chaque 1000 implusion

                counter=0;      //chaque sec le compteur repasse � 0
            }
        }
    }

